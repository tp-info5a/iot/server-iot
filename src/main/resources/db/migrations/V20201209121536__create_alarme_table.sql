create table alarme
(
    id int auto_increment,
    address_ip varchar(255) not null,
    etat varchar(255) not null,

    constraint user_pk primary key (id)
);

create table subscription
(
    user_id int not null,
    alarme_id int not null,

    constraint subscription_pk primary key (user_id, alarme_id),
    constraint subscription_user_fk foreign key (user_id) references user(id),
    constraint subscription_alarme_fk foreign key (alarme_id) references alarme(id)
)
