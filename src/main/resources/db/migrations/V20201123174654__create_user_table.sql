create table user
(
    id int auto_increment,
    firstname varchar(255) null,
    lastname varchar(255) null,
    mail varchar(255) not null,
    gender varchar(255) null,
    password varchar(255) null,
    birthday date null,
    address varchar(255) null,
    city varchar(255) null,
    country varchar(255) null,

    constraint user_pk primary key (id),
    constraint user_mail_uk unique (mail)
);
