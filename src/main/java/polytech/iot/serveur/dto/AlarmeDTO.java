package polytech.iot.serveur.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.iot.serveur.models.EtatEnum;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AlarmeDTO {
    private Integer id;
    private EtatEnum etat;
}
