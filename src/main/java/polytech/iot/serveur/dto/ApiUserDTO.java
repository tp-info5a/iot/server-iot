package polytech.iot.serveur.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.iot.serveur.models.ApiUser;
import polytech.iot.serveur.models.GenderEnum;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ApiUserDTO {
    private Integer id;
    private String firstname;
    private String lastname;
    private String mail;
    private GenderEnum gender;
    private LocalDate birthday;
    private String address;
    private String city;
    private String country;

    public ApiUserDTO(ApiUser apiUser) {
        this.id = apiUser.getId();
        this.firstname = apiUser.getFirstname();
        this.lastname = apiUser.getLastname();
        this.mail = apiUser.getMail();
        this.gender = apiUser.getGender();

    }
}
