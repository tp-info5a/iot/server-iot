package polytech.iot.serveur.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import polytech.iot.serveur.exceptions.ApiException;
import polytech.iot.serveur.utils.models.AlarmeEvent;
import polytech.iot.serveur.services.AlarmeService;

@RestController
@RequestMapping("/api/alarme")
public class AlarmeController {

    private final AlarmeService alarmeService;

    public AlarmeController(AlarmeService alarmeService) {
        this.alarmeService = alarmeService;
    }

    @RequestMapping(value="/login",method= RequestMethod.POST)
    public ResponseEntity<String> subscribe(@RequestBody String alarmeAddressIp) {
        Integer id = this.alarmeService.login(alarmeAddressIp);
        return ResponseEntity.ok("{\"id\": "+id+"}");
    }

    @RequestMapping(value="/unsubscribe/{id}",method= RequestMethod.GET)
    public ResponseEntity<Boolean> unsubscribe(@PathVariable("id") Integer id) {
        this.alarmeService.deleteAlarmeById(id);
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @RequestMapping(value="/update",method= RequestMethod.POST)
    public ResponseEntity<Boolean> update(@RequestBody AlarmeEvent alarmeEvent) throws ApiException {
        alarmeService.updateAlarme(alarmeEvent);
        return ResponseEntity.ok(Boolean.TRUE);
    }
}
