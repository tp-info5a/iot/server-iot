package polytech.iot.serveur.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import polytech.iot.serveur.dto.AlarmeDTO;
import polytech.iot.serveur.dto.ApiUserDTO;
import polytech.iot.serveur.exceptions.ApiException;
import polytech.iot.serveur.exceptions.ApiExceptionEnum;
import polytech.iot.serveur.models.Alarme;
import polytech.iot.serveur.utils.models.AlarmeEvent;
import polytech.iot.serveur.services.AlarmeService;
import polytech.iot.serveur.utils.models.Login;
import polytech.iot.serveur.models.ApiUser;
import polytech.iot.serveur.services.UserService;
import polytech.iot.serveur.utils.jwt.JwtUtil;
import polytech.iot.serveur.utils.models.UserLoginResponse;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private final ModelMapper modelMapper;

    private final AlarmeService alarmeService;
    private final UserService userService;

    private final JwtUtil jwtUtil;

    public UserController(UserService userService, JwtUtil jwtUtil, ModelMapper modelMapper, AlarmeService alarmeService) {
        this.userService = userService;
        this.jwtUtil = jwtUtil;
        this.modelMapper = modelMapper;
        this.alarmeService = alarmeService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<UserLoginResponse> login(@RequestBody Login login) throws ApiException {
        ApiUser apiUser = this.userService.login(login);
        UserDetails userDetails = this.jwtUtil.loadUser(apiUser);
        String token = this.jwtUtil.generateToken(userDetails);
        UserLoginResponse userLoginResponse = new UserLoginResponse(new ApiUserDTO(apiUser), token);
        return ResponseEntity.ok(userLoginResponse);
    }

    @RequestMapping(value = "/get-token", method = RequestMethod.POST)
    public ResponseEntity<String> getToken(@RequestBody Integer userId) throws ApiException {
        ApiUser apiUser = this.userService.getUserById(userId);
        UserDetails userDetails = this.jwtUtil.loadUser(apiUser);
        String token = this.jwtUtil.generateToken(userDetails);
        return ResponseEntity.ok("{\"token\" : \"" + token+ "\"}");
    }

    @RequestMapping(value = "/inscription", method = RequestMethod.POST)
    public ResponseEntity<UserLoginResponse> signInUSer(@RequestBody ApiUser newUser) throws ApiException {
        ApiUser apiUser = this.userService.inscription(newUser);
        UserDetails userDetails = this.jwtUtil.loadUser(apiUser);
        String token = this.jwtUtil.generateToken(userDetails);
        UserLoginResponse userLoginResponse = new UserLoginResponse(new ApiUserDTO(apiUser), token);
        return ResponseEntity.ok(userLoginResponse);
    }

    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ResponseEntity<ApiUserDTO> getUserDetail() throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);

        return ResponseEntity.ok(this.modelMapper.map(apiUser, ApiUserDTO.class));
    }

    @RequestMapping(value="/subscribe",method= RequestMethod.POST)
    public ResponseEntity<Boolean> subscribe(@RequestBody Integer alarmeId) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);
        alarmeService.subscribe(apiUser, alarmeId);
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @RequestMapping(value="/unsubscribe",method= RequestMethod.POST)
    public ResponseEntity<Boolean> unsubscribe(@RequestBody Integer alarmeId) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);
        alarmeService.unsubscribe(apiUser, alarmeId);
        return ResponseEntity.ok(Boolean.TRUE);
    }

    @RequestMapping(value="/get-subscribed-alarmes",method= RequestMethod.GET)
    public ResponseEntity<List<AlarmeDTO>> getSubscribedAlarmes() throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);
        List<AlarmeDTO> alarmeDTOList = apiUser.getAlarmes()
                .stream()
                .map(alarme -> new AlarmeDTO(alarme.getId(), alarme.getEtat()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(alarmeDTOList);
    }

    @RequestMapping(value="/update-alarme",method= RequestMethod.POST)
    public ResponseEntity<AlarmeDTO> updateAlarme(@RequestBody AlarmeEvent alarmeEvent) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);
        Boolean userSubscribedToAlarme = this.userService.isUserSubscribedToAlarme(apiUser, alarmeEvent.getId());
        if (!userSubscribedToAlarme) {
            throw new ApiException(HttpStatus.FORBIDDEN, ApiExceptionEnum.ERR_403_CANTUPDATEALARME);
        }
        Alarme alarme = this.alarmeService.updateAlarme(alarmeEvent);
        this.alarmeService.informAlarme(alarme);
        return ResponseEntity.ok(this.modelMapper.map(alarme, AlarmeDTO.class));
    }

    @RequestMapping(value="/alarme-status",method= RequestMethod.POST)
    public ResponseEntity<AlarmeDTO> alarmeStatus(@RequestBody Integer alarmeId) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.userService.loadUserByUsername(username);
        Alarme alarme = this.userService.getAlarmeStatus(apiUser, alarmeId);
        return ResponseEntity.ok(this.modelMapper.map(alarme, AlarmeDTO.class));
    }
}
