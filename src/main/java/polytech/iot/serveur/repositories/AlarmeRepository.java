package polytech.iot.serveur.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import polytech.iot.serveur.models.Alarme;


@Repository
public interface AlarmeRepository extends JpaRepository<Alarme,Integer> {
}
