package polytech.iot.serveur.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import polytech.iot.serveur.models.ApiUser;

import java.util.List;

@Repository
public interface ApiUserRepository extends JpaRepository<ApiUser, Integer> {

    List<ApiUser> findAllByMail(String mail);
}
