package polytech.iot.serveur.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import polytech.iot.serveur.exceptions.ApiException;
import polytech.iot.serveur.exceptions.ApiExceptionEnum;
import polytech.iot.serveur.models.*;
import polytech.iot.serveur.repositories.AlarmeRepository;
import polytech.iot.serveur.utils.models.AlarmeEtat;
import polytech.iot.serveur.utils.models.AlarmeEvent;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@Service
public class AlarmeService {
    private final AlarmeRepository alarmeRepository;

    private final RestTemplate template;

    public AlarmeService(AlarmeRepository alarmeRepository) {
        this.alarmeRepository = alarmeRepository;
        this.template = new RestTemplate();
    }

    public void subscribe(ApiUser apiUser, Integer alarmeId) throws ApiException {
        Optional<Alarme> optionalAlarme = alarmeRepository.findById(alarmeId);
        if(optionalAlarme.isEmpty()){
            throw new ApiException(HttpStatus.NOT_FOUND,"cannot find alarme with id:" + alarmeId);
        }

        List<ApiUser> liste = optionalAlarme.get().getUtilisateurs();
        if (liste.contains(apiUser)) {
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, ApiExceptionEnum.ERR_500_USERALREADYSUBCRIBED);
        }
        liste.add(apiUser);
        optionalAlarme.get().setUtilisateurs(liste);
        this.alarmeRepository.saveAndFlush(optionalAlarme.get());
    }

    public void unsubscribe(ApiUser apiUser, Integer alarmeId)throws ApiException{
        Optional<Alarme> alarme = alarmeRepository.findById(alarmeId);
        if(alarme.isEmpty()){
            throw new ApiException(HttpStatus.NOT_FOUND,ApiExceptionEnum.ERR_404_ALARMENOTFOUND, alarmeId.toString());
        }
        List<ApiUser> liste = alarme.get().getUtilisateurs();
        liste.remove(apiUser);
        alarme.get().setUtilisateurs(liste);
        this.alarmeRepository.saveAndFlush(alarme.get());
    }

    public Integer login(String alarmeAddressIp) {
        Alarme alarme = new Alarme();
        alarme.setAdresseIP(alarmeAddressIp);
        alarme.setEtat(EtatEnum.MARCHE);
        this.alarmeRepository.saveAndFlush(alarme);

        return alarme.getId();
    }

    public Alarme updateAlarme(AlarmeEvent alarmeEvent) throws ApiException {
        Optional<Alarme> optionalAlarme = this.alarmeRepository.findById(alarmeEvent.getId());
        if (optionalAlarme.isEmpty()) {
            throw new ApiException(HttpStatus.NOT_FOUND, ApiExceptionEnum.ERR_404_ALARMENOTFOUND, alarmeEvent.getId().toString());
        }

        Alarme alarme = optionalAlarme.get();
        alarme.setEtat(alarmeEvent.getEtat());
        this.alarmeRepository.saveAndFlush(alarme);

        return alarme;
    }

    public void deleteAlarmeById(Integer id) {
        this.alarmeRepository.deleteById(id);
    }

    public void informAlarme(Alarme alarme) throws ApiException {
        URI uri = URI.create("http://" + alarme.getAdresseIP() + "/webhook/alarme/intrusion");
        AlarmeEtat alarmeEtat = new AlarmeEtat(alarme.getId(), alarme.getEtat().toString());
        RequestEntity<AlarmeEtat> request = RequestEntity.post(uri).body(alarmeEtat);
        try {
            ResponseEntity<String> exchange = this.template.exchange(request, String.class);
        } catch (Exception e) {
            throw new ApiException(HttpStatus.SERVICE_UNAVAILABLE, ApiExceptionEnum.ERR_503_CANTCONNECTTOALARME);
        }
    }
}
