package polytech.iot.serveur.services;

import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import polytech.iot.serveur.exceptions.ApiException;
import polytech.iot.serveur.exceptions.ApiExceptionEnum;
import polytech.iot.serveur.models.Alarme;
import polytech.iot.serveur.models.ApiUser;
import polytech.iot.serveur.repositories.AlarmeRepository;
import polytech.iot.serveur.repositories.ApiUserRepository;
import polytech.iot.serveur.utils.models.Login;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final ApiUserRepository apiUserRepository;
    private final AlarmeRepository alarmeRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(ApiUserRepository apiUserRepository, PasswordEncoder passwordEncoder, AlarmeRepository alarmeRepository) {
        this.apiUserRepository = apiUserRepository;
        this.passwordEncoder = passwordEncoder;
        this.alarmeRepository = alarmeRepository;
    }

    public ApiUser login(Login login) throws ApiException {
        Optional<ApiUser> optionalUser = this.apiUserRepository.findAllByMail(login.getMail()).stream().findFirst();

        if (optionalUser.isEmpty())
            throw new ApiException(HttpStatus.NOT_FOUND, ApiExceptionEnum.ERR_404_USERNOTFOUND);

        String encodedPassword = optionalUser.get().getPassword();

        if (!this.passwordEncoder.matches(login.getPassword(), encodedPassword))
            throw new ApiException(HttpStatus.NOT_FOUND, ApiExceptionEnum.ERR_404_USERNOTFOUND);

        return optionalUser.get();
    }

    public ApiUser loadUserByUsername(String username) throws ApiException {
        Optional<ApiUser> optionalApiUser = this.apiUserRepository.findAllByMail(username).stream().findFirst();

        if (optionalApiUser.isEmpty())
            throw new ApiException(HttpStatus.NOT_FOUND, ApiExceptionEnum.ERR_404_USERNOTFOUND);

        return optionalApiUser.get();
    }

    public ApiUser inscription(ApiUser newUser) throws ApiException {
        List<ApiUser> allByEmail = this.apiUserRepository.findAllByMail(newUser.getMail());

        if (!allByEmail.isEmpty())
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, ApiExceptionEnum.ERR_500_USEREMAILNOTUNIQUE);

        newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));

        ApiUser save = this.apiUserRepository.saveAndFlush(newUser);
        this.apiUserRepository.flush();
        return save;
    }

    public Boolean isUserSubscribedToAlarme(ApiUser apiUser, Integer alarmeId) throws ApiException {
        Optional<Alarme> alarme = alarmeRepository.findById(alarmeId);
        if(alarme.isEmpty()){
            throw new ApiException(HttpStatus.NOT_FOUND,"cannot find alarme with id:" + alarmeId);
        }
        return alarme.get().getUtilisateurs().stream().anyMatch(apiUser1 -> apiUser1.getId().equals(apiUser.getId()));
    }

    public ApiUser getUserById(Integer userId) throws ApiException {
        Optional<ApiUser> optionalApiUser = this.apiUserRepository.findById(userId);
        if (optionalApiUser.isEmpty()) {
            throw new ApiException(HttpStatus.NOT_FOUND, ApiExceptionEnum.ERR_404_USERNOTFOUND);
        }

        return optionalApiUser.get();
    }

    public Alarme getAlarmeStatus(ApiUser apiUser, Integer alarmeId) throws ApiException {
        Optional<Alarme> optionalAlarme = apiUser.getAlarmes()
                .stream()
                .filter(alarme -> alarme.getId().equals(alarmeId))
                .findFirst();

        if (optionalAlarme.isEmpty())
            throw new ApiException(HttpStatus.INTERNAL_SERVER_ERROR, ApiExceptionEnum.ERR_403_CANTGETALARMESTATUS);

        return optionalAlarme.get();
    }
}
