package polytech.iot.serveur.models;

public enum GenderEnum {
    HOMME("homme"),
    FEMME("femme");

    private final String sexe;

    GenderEnum(String sexe) {
        this.sexe = sexe;
    }
}
