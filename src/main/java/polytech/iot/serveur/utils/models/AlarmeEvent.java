package polytech.iot.serveur.utils.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.iot.serveur.models.EtatEnum;

@NoArgsConstructor
@Getter
@Setter
public class AlarmeEvent {
    private Integer id;
    private EtatEnum etat;
}
