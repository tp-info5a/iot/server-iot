package polytech.iot.serveur.utils.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Login {
    private String mail;
    private String password;
}
