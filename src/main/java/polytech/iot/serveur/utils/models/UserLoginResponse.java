package polytech.iot.serveur.utils.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.iot.serveur.dto.ApiUserDTO;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserLoginResponse {
    private ApiUserDTO user;
    private String token;
}
