package polytech.iot.serveur.utils.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class LinkUserAlarme {
    private Integer idApiUser;
    private Integer idAlarme;
}
