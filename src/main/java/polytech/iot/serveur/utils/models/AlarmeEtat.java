package polytech.iot.serveur.utils.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AlarmeEtat {
    private Integer alarmeId;
    private String etat;
}
