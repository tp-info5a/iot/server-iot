package polytech.iot.serveur.exceptions;

import lombok.Getter;

@Getter
public enum ApiExceptionEnum {

    ERR_403_CANTUPDATEALARME("Vous ne pouvez pas modifier une alarme que vous ne connaissez pas."),
    ERR_403_CANTGETALARMESTATUS("Vous ne pouvez pas avoir les informations d'une alarme que vous ne connaissez pas."),

    ERR_404_USERNOTFOUND("Aucun utilisateur trouvé pour ce couple login."),
    ERR_404_ALARMENOTFOUND("Aucune alarme trouvée pour l'id "),

    ERR_500_USEREMAILNOTUNIQUE("Un autre utilisateur utilise déjà cette adresse mail."),
    ERR_500_USERALREADYSUBCRIBED("Vous êtes déjà abonné à cette alarme."),
    ERR_503_CANTCONNECTTOALARME("La connexion à l'alarme est impossible");

    private final String message;

    ApiExceptionEnum(String message) {
        this.message = message;
    }
}
