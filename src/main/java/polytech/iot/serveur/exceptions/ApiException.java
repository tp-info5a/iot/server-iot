package polytech.iot.serveur.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ApiException extends Exception {
    private final HttpStatus status;

    public ApiException(HttpStatus status, String message) {
        super(message);
        this.status = status;
    }

    public ApiException(HttpStatus status, ApiExceptionEnum apiExceptionEnum) {
        this(status, apiExceptionEnum.getMessage());
    }

    public ApiException(HttpStatus status, ApiExceptionEnum apiExceptionEnum, String detail) {
        this(status, apiExceptionEnum.getMessage() + detail);
    }
}
